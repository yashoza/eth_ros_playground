#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>


void chatterCallback(const sensor_msgs::LaserScan::ConstPtr& scan){

	//ROS_INFO("I heard: [%s]", msg->data.c_str());
	//ROS_INFO("I heard something");

	// Find the closest range between the defined minimum and maximum angles
	int minIndex = ceil((-1.0472 - scan->angle_min) / scan->angle_increment);
	int maxIndex = floor((1.0472 - scan->angle_max) / scan->angle_increment); 
	float closestRange = scan->ranges[minIndex];

	for (int currIndex = minIndex + 1; currIndex <= maxIndex; currIndex++) {
		if (scan->ranges[currIndex] < closestRange) {
			closestRange = scan->ranges[currIndex];
		}
	} 

	ROS_INFO_STREAM("Closest range: " << closestRange); 

}


int main(int argc, char **argv){

	ros::init(argc, argv, "scan_subscriber");

	ros::NodeHandle nh;

	int queue_size;

	std::string topic_name;

	nh.getParam("topic_name", topic_name);

	nh.getParam("queue_size", queue_size);

	//ROS_INFO("I heard something IN THE MAIN FUNTION");


	//ros::Subscriber sub = nh.subscribe("/scan", 1000, chatterCallback);
	ros::Subscriber sub = nh.subscribe("/scan", queue_size, chatterCallback);


	ros::spin();

	return 0;
}
