#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/yashoza/eth_ros_playground/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/yashoza/eth_ros_playground/devel/lib:/home/yashoza/ros_playground/devel/lib:/opt/ros/kinetic/lib:/opt/ros/kinetic/lib/x86_64-linux-gnu:/usr/local/cuda-8.0/lib64"
export PKG_CONFIG_PATH="/home/yashoza/eth_ros_playground/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/home/yashoza/eth_ros_playground/build"
export ROSLISP_PACKAGE_DIRECTORIES="/home/yashoza/eth_ros_playground/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/yashoza/eth_ros_playground/src:$ROS_PACKAGE_PATH"